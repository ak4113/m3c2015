#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv
#gfortran -c fdmodule.f90 advmodule2d.f90 ode2d.f90
#f2py -llapack -c advmodule2d.f90 fdmodule.f90 fdmodule2d.f90 ode2d.f90 -m adv --f90flags='-fopenmp' -lgomp

def advection1f(nt,tf,n,S=1.0,display=False,numthreads=1.0,par=1.0):
   
    x0=0.5
    y0=0.5
    t0=0.0
    adv.nt=nt
    adv.t0=t0
    adv.dt=dt
    adv.advmodule.c1_adv=1.0
    adv.advmodule.c2_adv=1.0
    adv.advmodule.s_adv=S
    adv.advmodule.S_adv=S
    adv.numthreads=numthreads
    adv.ode2d.par=par
    

    f0=np.empty([n,n])
    x = np.arange(0.0,n*dx,dx)
    y = np.arange(0.0,n*dx,dx)
    X, Y =np.meshgrid(x,y)
    f0 = np.exp(-100.0*((X-x0)**2+(Y-y0)**2))
    #advection equation in 2 dimentions
    adv.y0=f0
    f=adv.ode2d.euler(t0,f0,dt,nt)
    f4,time=adv.ode2d.rk4(t0,f0,dt,nt)
    
    if display:
       plt.figure()
      # CS= plt.contour(X, Y, f)
       CS1= plt.contour(X, Y, f4)
      # plt.clabel(CS, inline=1, fontsize=10)
       plt.clabel(CS1, inline=1, fontsize=10)
       plt.title('Simplest default with labels')
    
    return f, f4, time
    

 
def test_advection1f(n):
    """We now will asses the accuracy of solution computation of 2D advection equation when c1=c2=1 and t=1, producing the trend of error change with number of computations n, as well as displaying countour plots for Euler and Runge-Kutta solutions. 
Finally, in order to asses the speedup of the parralel code, we compare calls to advection1f, modifying calls to RHS with grad2d and grad2d_omp, by means of changing par=1/0 accordingly. 
    """
    e = [] 
    e4 = []
    time = []
    time_p = []
    dx = 1.0/float(n)
    x0=0.5
    y0=0.5
    S=1.0
    nvalues = np.array([50, 100, 150, 200])
    
    for n in nvalues:
        f,f4,time1 = advection1f(nt,tf,n,S=1.0,display=False,par=1.0)
        f0=np.empty([n,n])
        x = np.arange(0.0,n*dx,dx)
        y = np.arange(0.0,n*dx,dx)
        X, Y =np.meshgrid(x,y)
        f0 = np.exp(-100.0*((X-x0)**2+(Y-y0)**2))
        e = e + [np.mean(np.abs(f-(f0+S*tf)))]
        e4 = e4 + [np.mean(np.abs(f4-(f0+S*tf)))]
        time = time + [time1]
        
        f,f4,time2 = advection1f(nt,tf,n,S=1.0,display=False,par=0.0)
        time_p = time_p + [time2]
    m,p = np.polyfit(np.log(nvalues),np.log(e),1)
    plt.figure()
    plt.loglog(nvalues,e,nvalues,e4)
    plt.xlabel('n')
    plt.ylabel('error')
    plt.title('Alina test_advection1, variation of error with n for c=S=1, dx=1/n')
    plt.loglog(nvalues,np.exp(p)*nvalues**m,'--')
    plt.axis('tight')
    plt.savefig("errors.png")

    speedup = np.empty(4)
    for i in range(4): 
        speedup[i] = time_p[i]/time[i]
    
    plt.figure()
    plt.plot(nvalues,speedup)
    plt.legend(('n=50','100','150','200'),loc='best')
    plt.xlabel('n')
    plt.ylabel('speedup')
    plt.title('Alina Khay, test_gradN')
    plt.axis('tight')
    """
    As expected, the error decreases with number of computations and we can also observe the linear least squares plot. The ratios of computational time of parallelised and non-parallelised code have been plotted and we can observe that call to grad2d_omp version is always(except for one point) takes less time than non-parallel version of it(ratio values stay between 0 and 1).
    Contour plot displays the solution to the advection equation in 2 spacial dimentions.
    """
    return e,e4,time,time_p
  
if __name__=='__main__':
    n = 50
    nt = 32000
    tf = 1.21
    dt = tf/float(nt)
    dx = 1.0/float(n)
    f,f4,t = advection1f(nt,tf,n,1.0,True,1.0)
    e,e4,t,tp = test_advection1f(50)
    print e,e4
    plt.show()

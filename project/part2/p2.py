"""Project part 2
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from p2 import fdmodule as f1
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display,savefig):
    """set all elements of M to zero where grad_amp < fac*max(grad_amp)
    """
    f2d.dx1=1
    f2d.dx2=1
    dM1=np.empty((768,1024,3))
    dM2=np.empty((768,1024,3))
    grad_amp=np.empty((768,1024,3))
    df_max=np.empty(3)
    for i in range(3):
        dM1[:,:,i],dM2[:,:,i],grad_amp[:,:,i],df_max[i]=f2d.grad_omp(M[:,:,i])

        
    M[grad_amp<fac*df_max]=0
    
    if display:
       plt.figure()
       plt.imshow(M)
    if savefig:
       plt.savefig("p2.png")
    
    return M

if __name__ == '__main__':
     M=misc.face()
     L=misc.face()
     print "shape(M):",np.shape(M)
     plt.figure()
     plt.imshow(M)
     plt.show() #may need to close figure for code to continue
     N,dfmax=threshold(M,0.2,True,True) #Uncomment this line 
     plt.show()
    

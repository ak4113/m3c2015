"""Project part 3, solve the 1d advection equation in fortran"""
import numpy as np
import matplotlib.pyplot as plt
import adv
#gfortran -c fdmodule.f90 advmodule.f90 ode.f90
#f2py -llapack -c fdmodule.f90 ode.f90 advmodule.f90 -m adv --f90flags='-fopenmp' -lgomp

def advection1f(nt,tf,n,dx,c=1.0,S=0.0,display=True,numthreads=1):
    """solve advection equation, df/dt + c df/dx = S
    for x=0,dx,...,(n-1)*dx, and returns f(x,tf),fp(x,tf),
    and f4(x,tf) which are solutions obtained using the fortran
    routines ode_euler, ode_euler_omp, ode_rk4
    f(x,t=0) = sin(2 pi x/L), L = n*dx
    The solutions are plotted if display is true
    """
    t0=0.0
    adv.nt=nt
    adv.t0=t0
    adv.dt=dt
    adv.advmodule.c_adv=c
    adv.advmodule.s_adv=S
    adv.numthreads=numthreads
    L = n*dx
    x = np.arange(0.0,n*dx,dx)
    f0 = np.sin(2*np.pi*x/L)
    adv.y0=f0
    
    f=adv.ode.euler(t0,f0,dt,nt)
    fp=adv.ode.euler_omp(t0,f0,dt,nt,numthreads)
    f4=adv.ode.rk4(t0,f0,dt,nt)
    if display:
        plt.figure()
        plt.plot(x,f,x,fp,x,f4)
        plt.xlabel('x')
        plt.ylabel('f')
        plt.title('Alina Khaybullina advection1 \n advection eqn. solution for n,dx,c,S=%d,%2.3f,%2.1f,%2.1f' %(n,dx,c,S))
        plt.grid()
        plt.axis('tight')
    return f, fp, f4
    

 
def test_advection1f(n):
    """compute scaled L1 errors for solutions with n points 
    produced by advection1f when c=1,S=1,tf=1,nt=16000, L=1"""  
    t0=0.0
    S=1.0
    nt=16000
    c=1.0
    L=1.0
    dx = 1.0/float(n)
    x = np.arange(0.0,n*dx,dx)
    f0 = np.sin(2*np.pi*(x-c*tf)/L)
    
    f,fp,f4 = advection1f(nt,tf,n,dx,c=1.0,S=1.0,display=False)
    
    e=np.mean(np.abs(f-(f0+S*tf)))  
    ep=np.mean(np.abs(fp-(f0+S*tf)))
    e4=np.mean(np.abs(f4-(f0+S*tf)))
      
    return e,ep,e4 #errors from euler, euler_omp, rk4
        

if __name__ == '__main__':
    n = 400
    nt = 16000
    tf = 1.21
    dt = tf/float(nt)
    dx = 1.0/float(n)
    f,fp,f4 = advection1f(nt,tf,n,dx,1.0,1.0,True)
    e,ep,e4 = test_advection1f(200)
    eb,epb,e4b = test_advection1f(400)
    eb1,epb1,e4b1 = test_advection1f(600)
    print e,ep,e4
    print eb,epb,e4b
    print eb1,epb1,e4b1
    print e/eb,ep/epb,e4/e4b
    plt.show()

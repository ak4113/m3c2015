"""Exercise 4: complete the script below to solve the
    2nd order ode:
    y'' - mu*(1-y^2)*y' + y = 0
    """
#Step 0: Browse through the code, how does it compare to ode_example.py from lecture?

#Step 1: write  the 2nd order ode as two 1st order ode's (this has been done for you):
#dz1 = z2
#dz2 = mu*(1-z1**2)*z2 - z1
#Compare the equation for dz2 to the original ODE

import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import odeint

def rhs(Y,t,mu):

    #Step 2: Since y0 below (see step 5) has two components, Y also has two components.
    #Assign those two components to the variables z1 and z2:
    #z1=?
    #z2=?

    #Step 3: Calculate dz1 and dz2 based on what you wrote for step 1
    #dz1=?
    #dz2=?

    #Step 4: store dz1 and dz2 in a tuple, dY
    #dY=?
    return dY
  

#some ODE solvers use the Jacobian of the RHS of the ODE
#this (optional) function can be provided to the solver via the Dfun optional
#argument in the call to odeint below
def jac(Y,t,mu):
    J = [[0,1],[-2*mu*Y[0]*Y[1],-1]]
    return J
    
        
t = np.linspace(0,20,10001)

#Step 5: set the intitial conditions to y(0)=1, dy(0) = 0
#y0 should be a two-item tuple (or list)
#y0 = ?

mu=2.0 #parameter in ODE

#call to odeint
Yout,info = odeint(rhs,y0,t,args=(mu,),Dfun=jac,full_output=True)


#Step 6: Yout is a 2d array, extract z1 and z2 from Yout:
#z1 = ?
#z2 = ?

#display solution
plt.figure()
plt.plot(t,z1,t,z2)
plt.xlabel('t')
plt.ylabel('y,dy')
plt.legend(('y','dy'))


plt.figure()
plt.plot(z1,z2)
plt.xlabel('y')
plt.ylabel('dy')
plt.show()

#Step 7: Run the code!

#Step 8 (Optional)  Add a forcing term to the RHS of the ODE, A*sin(w*t) where A and w are
#new parameters that have to be sent into the rhs. Try mu=8.53, A=1.2, and w = pi/5

#Step 9 (Optional): Add a for loop which loops through a few different values of mu
#modify the plotting code to compare these solutions.

""" M3C 2015 Homework 1
Python script which prints name and college id
To run this code, enter "python hw1.py" at the terminal
"""

#1. modify the list, Output, so that it contains your name and college id
Output = ["Alina Khaybullina","00832476"]

#2. modify x and y in the print statements below so that your name and college id are output
print "M3C 2015 Homework 1 by", Output[0]
print "CID:", Output[1]
